<?php
Class Login_model extends CI_Model
{

 function login($username,$password)
    {
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where('UserName',$username);
        $this->db->where('UserPassword',$password);
        $query = $this->db->get();
        return $query->result();
    }
     function saveadminnotifications($adminnotifications)
    {
        $this->db->insert('notification',$adminnotifications);
        return 1;
    }
     function saveadminusers($admiusers)
    {
        $this->db->insert('login',$admiusers);
        return 1;
    }
     function getallnotifications()
    {
       	$this->db->select('*');
        $this->db->from('notification');
        // $this->db->where('UserName',$username);
        // $this->db->where('UserPassword',$password);
        $query = $this->db->get();
        return $query->result();
    }
    function getallprayerrequests()
    {   
        $status = 1;
        $this->db->select('*');
        $this->db->from('prayerrequest');
        $this->db->where('Status',$status);
        $query = $this->db->get();
        return $query->result();
    }
    function updateprayerstatus($prayerrequestid)
    {   
        $updateprayerstatus =array (
            'Status' =>0
            );
        $this->db->where('prayerrequest.PrayerRequestId',$prayerrequestid);
        return $this->db->update('prayerrequest',$updateprayerstatus);

    }
    function deleteprayerrequest($prayerrequestid)
    {
        $this->db->where('PrayerRequestId',$prayerrequestid); 
        $this->db->delete('prayerrequest'); 
        return 1;
    }
    function getallprayercount()
    {
        $this->db->select('*');
        $this->db->from('prayercount');
        $this->db->join('registration', 'prayercount.UserId = registration.RegisterId');
        $query = $this->db->get();
        return $query->result();
    }
    function saveadminweblinks($weblink,$linkdetails)
    {   

        $this->db->where('weblink.LinkNo',$weblink);
        $this->db->update('weblink',$linkdetails);
        return 1;

    }
    function getallweburls()
    {
        $this->db->select('*');
        $this->db->from('weblink');
        $query = $this->db->get();
        return $query->result();
    }
    function getlinkdetails($link) 
    {
        $this->db->select('*');
        $this->db->from('weblink');
        $this->db->where('LinkNo',$link);
        $query = $this->db->get();
        return $query->result();
    }
    function getnotificationdetails($notificationid) 
    {
        $this->db->select('*');
        $this->db->from('notification');
        $this->db->where('NotificationId',$notificationid);
        $query = $this->db->get();
        return $query->result();
    }
    function updatenotificationdetails($updatenotificationdetails,$editednotificationid)
    {
        $this->db->where('notification.NotificationId',$editednotificationid);
        $this->db->update('notification',$updatenotificationdetails);
        return 1;
    }
    function deletenotification($deletenotificationid)
    {
        $this->db->where('NotificationId',$deletenotificationid); 
        $this->db->delete('notification'); 
        return 1;
    }
    function getuserdetails($getprayerrequestid)
    {
        $this->db->select('*');
        $this->db->from('prayerrequest');
        $this->db->where('PrayerRequestId',$getprayerrequestid);
        $query = $this->db->get();
        return $query->result();
    }
}
