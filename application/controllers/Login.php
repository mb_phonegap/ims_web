<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Login_model');

	}
	public function index()
	{
		// $this->load->view('header.php');
		$this->load->view('admin_login');
		// $this->load->view('footer.php');
	}

	function loginverification()
	{
			$username         = $_GET['uname'];
			$password         = $_GET['password'];

			$sucess           = '1';
			$error            = '2';
			$logindata        = $this->Login_model->login($username,$password);
			if($logindata    != null){
			foreach ($logindata as $value){
			$usertype    = $value->UserType;
			}

			$this->session->set_userdata('username',$username);
			$this->session->set_userdata('usertype',$usertype);
			$this->session->set_userdata('log','login');
			echo json_encode($sucess);
			}
			else
			{
			echo json_encode($error);
			}
	}
	 function addnotification()
    {  
        $result              = 2;
        $_GET                = json_decode(file_get_contents('php://input'), true);
        $notification   = $_GET['notification'];
        $date 			= date('Y-m-d H:i:s');
            $adminnotifications = array(
                'AdminNotifications' => $notification,
                'CreatedOn' => $date
            );
            $result              = $this->Login_model->saveadminnotifications($adminnotifications);

          	 echo json_encode($result);
           
    }
     function getallnotifications()
    {  
       
        $result = $this->Login_model->getallnotifications();
        echo json_encode($result);
    }
	function notification()
	{
			$this->load->view('header.php');
			$this->load->view('admin_notification');
			$this->load->view('footer.php');
	}
	function prayerrequest()
	{
			$this->load->view('header.php');
			$this->load->view('admin_prayerrequests');
			$this->load->view('footer.php');
	}
	function users()
	{
		$this->load->view('header.php');
		$this->load->view('admin_users');
		$this->load->view('footer.php');
	}
	function prayercount()
	{
		$this->load->view('header.php');
		$this->load->view('admin_userprayercount');
		$this->load->view('footer.php');
	}
	function weblink()
	{
		$this->load->view('header.php');
		$this->load->view('admin_weblink');
		$this->load->view('footer.php');
	}

	function getallprayerrequests()
	{
		$result = $this->Login_model->getallprayerrequests();
		echo json_encode($result);
	}
	function approveprayerrequest(){
		// echo "string";
		$prayerrequestid 	 = 	$_GET['prayerrequestid'];
		$updatedprayerstatus =  $this->Login_model->updateprayerstatus($prayerrequestid);
		echo json_encode($updatedprayerstatus);
	}
	function deleteprayer() 
	{
		$_GET                = json_decode(file_get_contents('php://input'), true);	
		$prayerrequestid   = $_GET['prayerrequestid'];
    	$result 				= $this->Login_model->deleteprayerrequest($prayerrequestid);	
    	echo json_encode($result);
	}

	 function addusers()
    {  
        //$result              = 2;
        $_GET                = json_decode(file_get_contents('php://input'), true);
        $username   = $_GET['username'];
        $password 	= $_GET['password'];
        //$date 			= date('Y-m-d H:i:s');
            $admiusers = array(
                'UserName' => $username,
                'UserPassword' => $password,
                'UserType' => 'user'
            );
            $result              = $this->Login_model->saveadminusers($admiusers);

          	 echo json_encode($result);
           
    }
     function saveweblink()
    {  
        //$result              = 2;
        $_GET            = json_decode(file_get_contents('php://input'), true);
        $weblink  	 = $_GET['weblink'];
        $linkname 	 = $_GET['linkname'];
        $linkurl   	 = $_GET['linkurl'];

        $linkdetails = array(
            'LinkName' => $linkname,
            'WebUrl'   => $linkurl
        );

        $result       = $this->Login_model->saveadminweblinks($weblink,$linkdetails);

        echo json_encode($result);
           
    }
     function getallprayercount()
    {  
        $result = $this->Login_model->getallprayercount();
        echo json_encode($result);
    }
     function getallweburls()
    {  
       
        $result = $this->Login_model->getallweburls();
        echo json_encode($result);
    }
    function getdetailsoflink()
    {
    	$_GET            = json_decode(file_get_contents('php://input'), true);
    	$firstlink  	 = $_GET['link'];
    	$result          = $this->Login_model->getlinkdetails($firstlink);
    	echo json_encode($result);
    }
    function getnotification()
    {
    	$_GET			= json_decode(file_get_contents('php://input'), true);
    	$notificationid = $_GET['notificationid'];
    	$result 		= $this->Login_model->getnotificationdetails($notificationid);	
    	echo json_encode($result);
    }
    function updatenotification()
    {
    	$_GET					= json_decode(file_get_contents('php://input'), true);
    	$editednotification 	= $_GET['editednotification'];
    	$editednotificationid 	= $_GET['editednotificationid'];
    	$updatenotificationdetails = array(
    		'AdminNotifications' => $editednotification
    		);
    	$result 				= $this->Login_model->updatenotificationdetails($updatenotificationdetails,$editednotificationid);	
    	echo json_encode($result);
    }
    function deletenotification() 
    {
    	$_GET					= json_decode(file_get_contents('php://input'), true);
    	$deletenotificationid   = $_GET['deletenotificationid'];
    	$result 				= $this->Login_model->deletenotification($deletenotificationid);	
    	echo json_encode($result);
    }
    function viewuserdetails() 
    {
    	$_GET 					= json_decode(file_get_contents('php://input'), true);
    	$getprayerrequestid		= $_GET['prayerrequestid'];	
    	$result 				= $this->Login_model->getuserdetails($getprayerrequestid);
    	echo json_encode($result);
    }
	function logout()
  	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('usertype');
		$this->session->set_userdata('log','logout');
		redirect('Login','refresh');
		//redirect('index','refresh'); 
		//$this->load->view('admin_login');
	}
}
