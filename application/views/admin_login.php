<!DOCTYPE html>
<html ng-app="imsAmma">
<head>
  <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ims Matha Login Form</title>
  
  
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.js"></script>

  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/sweet-alert.css" />
  <link rel="stylesheet" href="assets/css/loading-bar.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>

<body>

<hgroup>
   <h1>IMS MATHA ADMIN</h1>
   <!-- <h3>By Josh Adamous</h3> -->
</hgroup>
<div ng-controller="LoginController">
<form name="loginForm" ng-submit="Login.LoginCheck()" novalidate>

 <div class="form-group" ng-class="{'has-error': loginForm.email.$invalid && loginForm.email.$dirty, 'has-success': loginForm.email.$valid }">
    <input type="text" class="form-control" placeholder="Username" name="email" id="email" ng-model="user.email" required/>
    <p class="help-block" ng-if="loginForm.email.$invalid && loginForm.email.$dirty">Please Enter a valid email address</p>
  </div>
  <div class="group" ng-class="{'has-error': loginForm.password.$invalid && loginForm.password.$dirty, 'has-success': loginForm.password.$valid}">
    <input type="password" class="form-control" placeholder="Password" name="password" id="password" ng-model="user.password" ng-minLength="5" required/>
    <p class="help-block" ng-if="loginForm.password.$invalid && loginForm.password.$dirty">Please Enter at least 8 characters</p>
  </div>

   <div class="group">
   
   <button type="submit" class="button buttonBlue">
      Login
      <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
   </button>
   </div>
</form>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="../assets/js/index.js"></script>
     <script src="assets/js/ng/app.js"></script>
    <script src="assets/js/ng/Controller/login/LoginController.js"></script>
    <script src="assets/js/ng/Services/Login/LoginService.js"></script>
    <script src="assets/js/sweet-alert.min.js"></script>
     <script src="assets/js/loading-bar.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
