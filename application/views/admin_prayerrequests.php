<div class="container"  ng-controller="LoginController" ng-init="Login.getallprayerrequests()">
   <h2>Approve Prayer Requests</h2>
   <table class="table table-hover" ng-hide="Ishiddenprayerrequest">
      <thead>
         <tr>
            <th>Prayer Requests</th>
            <th></th>
            <th>Action</th>
            <!--  <th>Email Id</th>
               <th>City</th>
               <th>Phone Number</th>
               <th>Address</th> -->
         </tr>
      </thead>
      <tbody>
         <tr ng-repeat="prayer in prayerrequest track by $index">
            <td>{{prayer.UserPrayerRequest}}</td>
            <td><button type="button" class="btn btn-primary" ng-click="Login.viewprayer(prayer.PrayerRequestId);">View Profile details</button></td>
            <td><button type="button" class="btn btn-primary" ng-click="Login.approveprayer(prayer.PrayerRequestId);">Approve</button></td>
            <td>
               <button type="button" class="btn btn-primary" ng-click="Login.deleteprayerrequest(prayer.PrayerRequestId);">Delete / Disapprove</button>
               <!-- <a href="../Login/approveprayerrequest" ng-click="Login.approveprayer({{prayer.PrayerRequestId}})" target="_blank">approve</a> -->
            </td>
         </tr>
      </tbody>
   </table>
   <h3 ng-hide="Ishiddenrequests">No Prayer Request to Approve</h3>
   <div class="modal fade" id="viewusermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">User Details</h4>
                                        </div>
                                         <!-- <h2>Notfication</h2> -->
                                         <div class="modal-body">
                                         <div class="row form-group">
                                         <div class="col-md-6"><h4 id="modalpopuser">User Name</h4></div>
                                         <div class="col-md-6"><h4 id="modalpopuser">User Phonenumber</h4></div>
                                        </div>
                                        <div class="row form-group">
                                         <div class="col-md-6"><span id="modalpopuser">{{viewusername}}</span></div>
                                         <div class="col-md-6"><span id="modalpopuser">{{viewuserphonenumber}}</span></div>
                                        </div>
                                        
                                        <div class="row form-group">
                                         <div class="col-md-6"><h4 id="modalpopuser">User Email address</h4></div>
                                         <div class="col-md-6"><h4 id="modalpopuser">User Address</h4></div>
                                        </div>
                                        <div class="row form-group">
                                         <div class="col-md-6"><span id="modalpopuser">{{viewuseremailaddress}}</span></div>
                                         <div class="col-md-6"><span id="modalpopuser">{{viewuseraddress}}</span></div>
                                        </div>
                                        </div>
                                       <!--  <div class="form-group" id="div_notification">

      <textarea type="text" class="form-control" id="editnotification" placeholder="Enter notification" name="editnotification" ng-model="editnotification"></textarea>
    </div> -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <!-- <button type="button" class="btn btn-primary" ng-click="Login.updatenotification(editnotificationid);">Save changes</button> -->
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div> 
</div>
<!-- </div> -->
<script>
   $(document).ready(function () {
       $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
      });
    //your code here
   });
     
      
</script>