angular.module('imsAmma').factory('LoginService', ['$http', '$q', '$timeout', '$filter', '$rootScope', '$window', function($http, $q, $timeout, $filter, $rootScope, $window) {
    var LoginService = {
        getLogin: function(uname, password) {
            //debugger
            var url = 'Login/loginverification?uname=' + uname + '&password=' + password;
            return $http.get(url, {});
        },
        addnotification: function(notification) {
            //debugger
            return $http.post('../Login/addnotification', { notification: notification });
        },
        getallnotifications: function() {

            return $http.post('../Login/getallnotifications', {})
        },
        getallprayerrequests: function() {

            return $http.post('../Login/getallprayerrequests', {})
        },
        updateprayerstatus: function(prayerrequestid) {
            var url = '../Login/approveprayerrequest?prayerrequestid=' + prayerrequestid;
            return $http.get(url, {});
        },
        deleteprayer: function(prayerrequestid) {
            return $http.post('../Login/deleteprayer',{prayerrequestid:prayerrequestid})
            // var url = '../Login/deleteprayer?prayerrequestid=' + prayerrequestid;
            // return $http.get(url, {});
        },
        addusers: function(username, password) {
            return $http.post('../Login/addusers', { username: username, password: password });
        },
        getallprayercount: function() {
            return $http.post('../Login/getallprayercount', {})
        },
        saveweblink: function(weblink,linkname,linkurl) {
            return $http.post('../Login/saveweblink', {weblink:weblink,linkname:linkname,linkurl:linkurl})
        },
        getdetailsoflink: function(link) {
            return $http.post('../Login/getdetailsoflink', {link:link})
        },
        getallweburls: function() {
            return $http.post('../Login/getallweburls', {})
        },
        getnotification: function(notificationid) {
            return $http.post('../Login/getnotification',{notificationid:notificationid})
        },
        updatenotification: function(editednotification,editednotificationid) {
            return $http.post('../Login/updatenotification',{editednotification:editednotification,editednotificationid:editednotificationid})
        },
        deletenotification: function(deletenotificationid){
            return $http.post('../Login/deletenotification',{deletenotificationid:deletenotificationid})
        },
        sendnotificationtoapp : function(){
            return $http.post('http://qikdots.com/imsapi/public/api/v1/sendnotification');
        },
        viewuserdetails: function(prayerrequestid){
            return $http.post('../Login/viewuserdetails',{prayerrequestid:prayerrequestid})
        }

    }
    return LoginService;
}]);
