imsAmma.controller('LoginController', ['$scope', '$rootScope', 'LoginService', '$window', '$location', '$http', '$timeout','cfpLoadingBar', function($scope, $rootScope, LoginService, $window, $location, $http, $timeout,cfpLoadingBar) {
    $scope.Login = {};
    // $scope.Register = {};

    (function(Login) {
        Login.check = [];
        Login.Credentials = {};
        $scope.Ishidden = true;
        $scope.Ishiddenprayerrequest = true;
        $scope.Ishiddenrequests = true;
        //$scope.displayLoadingIndicator = true;
        Login.LoginCheck = function() {
            cfpLoadingBar.start();
            //cfpLoadingBar.complete();
            //alert("hai");
            Login.UserName = $('#email').val();
            Login.Password = $('#password').val();
            if (Login.UserName != "" && Login.Password != "") {
                LoginService.getLogin(Login.UserName, Login.Password).success(function(response) {
                    Login.Check = JSON.parse(response);
                    cfpLoadingBar.complete();
                    if (Login.Check == 1) {
                        window.location.href = "../IMSMATHA/Login/Notification";
                    } else {
                        //cfpLoadingBar.complete();
                        swal({
                            title: "Failed!",
                            text: "Username or Password is incorrect"

                        });
                    }
                })
            } else {
                cfpLoadingBar.complete();
                swal({
                    title: "Failed!",
                    text: "Username or Password is empty"

                });
            }
        }
        Login.AddNotifications = function() {
            cfpLoadingBar.start();
            Login.Notification = $('#notification').val();
            if (Login.Notification) {
                LoginService.addnotification(Login.Notification).success(function(response) {
                    Login.Check = JSON.parse(response);
                    cfpLoadingBar.complete();
                    if (Login.Check == 1) {
                        //LoginService.addnotification(Login.Notification).success(function(response) {
                        swal({
                            title: "Success",
                            text: "Notification Added Successfully"
                        });
                        LoginService.sendnotificationtoapp().success(function(response) {
                        })
                        $('#notification').val('');
                        Login.getallnotifications();
                    } else {
                        swal({
                            title: "Failed!",
                            text: "Notification Added Failed"

                        });
                    }
                })
            } else {
                cfpLoadingBar.complete();
                swal({
                    title: "Failed!",
                    text: "Notification Field is empty"

                });
            }

        }
        Login.getallnotifications = function() {
            cfpLoadingBar.start();
            LoginService.getallnotifications().success(function(response) {
                cfpLoadingBar.complete();
                $scope.Ishidden = false;
                $scope.notifications = response;
            })
        }
        Login.getallprayerrequests = function() {
            cfpLoadingBar.start();
            LoginService.getallprayerrequests().success(function(response) {
                cfpLoadingBar.complete();
                if (response.length >= 1) {
                    $scope.Ishiddenprayerrequest = false;
                    $scope.prayerrequest = response;
                } else {
                    $scope.Ishiddenrequests = false;
                    $scope.Ishiddenprayerrequest = true;
                }

            })
        }
        Login.approveprayer = function(prayerrequestid) {
            cfpLoadingBar.start();
            $scope.prayerrequestid = prayerrequestid;
            LoginService.updateprayerstatus($scope.prayerrequestid).success(function(response) {
                cfpLoadingBar.complete();
                    Login.updatedprayerstatus = JSON.parse(response);
                    if (Login.updatedprayerstatus == true) {
                        swal({
                            title: "Success",
                            text: "Prayer Request has been Approved"
                        });
                        Login.getallprayerrequests();
                    } else {
                        swal({
                            title: "Failed!",
                            text: "Couldn't Grant approval"

                        });
                    }
                })
                //$scope.requestid = $scope.prayerrequestid;
                //alert("hai");

        }
        Login.deleteprayerrequest = function(prayerrequestid) {
            cfpLoadingBar.start();
            $scope.prayerrequestid = prayerrequestid;
            LoginService.deleteprayer($scope.prayerrequestid).success(function(response) {
                cfpLoadingBar.complete();
                    Login.deleteprayer = JSON.parse(response);
                    if (Login.deleteprayer == 1) {
                        swal({
                            title: "Success",
                            text: "Prayer Request has been Deleted"
                        });
                        Login.getallprayerrequests();
                    } else {
                        swal({
                            title: "Failed!",
                            text: "Prayer Request Deletion failed"

                        });
                    }
                })


        }

        Login.AddUsers = function() {
            cfpLoadingBar.start();
            Login.Username = $('#username').val();
            Login.Password = $('#password').val();
            if (Login.Username && Login.Password) {
                LoginService.addusers(Login.Username, Login.Password).success(function(response) {
                    cfpLoadingBar.complete();
                    Login.Check = JSON.parse(response);
                    if (Login.Check == 1) {
                        swal({
                            title: "Success",
                            text: "User Added Successfully"
                        });

                        $('#username').val('');
                        $('#password').val('');
                        //Login.getallnotifications();
                    } else {
                        swal({
                            title: "Failed!",
                            text: "User creation failed"

                        });
                    }
                })
            } else {
                cfpLoadingBar.complete();
                swal({
                    title: "Failed!",
                    text: "Fields are empty"

                });
            }
        }

        Login.getallprayercount = function() {
            cfpLoadingBar.start();
            LoginService.getallprayercount().success(function(response) {
                cfpLoadingBar.complete();
                $scope.prayercount = response;
            })
        }
        $scope.selectedlink1 = 'Link 1';

        Login.saveweblink = function() {
            cfpLoadingBar.start();
            $scope.weblink = $scope.selectedlink1;
            $scope.linkname = $scope.linkname;
            $scope.linkurl = $scope.linkurl;
            if ($scope.linkname && $scope.linkurl) {
                LoginService.saveweblink($scope.weblink, $scope.linkname, $scope.linkurl).success(function(response) {
                    cfpLoadingBar.complete();
                    Login.LinkCheck = JSON.parse(response);
                    if (Login.LinkCheck == 1) {
                        swal({
                            title: "Success",
                            text: "Web Link Updated Successfully"
                        });

                        Login.getallweburls();
                    } else {
                        swal({
                            title: "Failed!",
                            text: "User creation failed"

                        });
                    }
                })
            } else {
                cfpLoadingBar.complete();
                swal({
                    title: "Failed!",
                    text: "Fields are empty"

                });
            }
        }

        Login.getallweburls = function() {
            Login.linkchange();
            LoginService.getallweburls().success(function(response) {
                $scope.weburls = response;
            })
        }

        Login.linkchange = function() {
            cfpLoadingBar.start();
            //$scope.displayLoadingIndicator = true;
            $scope.link = $scope.selectedlink1;
            LoginService.getdetailsoflink($scope.link).success(function(response) {
                cfpLoadingBar.complete();
                //$scope.displayLoadingIndicator = false;
                $scope.linkname = response[0].LinkName;
                $scope.linkurl = response[0].WebUrl;
            })
        }

        Login.editnotification = function(notificationid) {
            cfpLoadingBar.start();
            $scope.getnotificationid = notificationid;
            LoginService.getnotification($scope.getnotificationid).success(function(response) {
                cfpLoadingBar.complete();
                $scope.editnotificationid = response[0].NotificationId;
                $scope.editnotification = response[0].AdminNotifications;
            })
        }

        Login.updatenotification = function(notificationid) {
            cfpLoadingBar.start();
            Login.editednotificationtext = $('#editnotification').val();
            if (Login.editednotificationtext) {
                Login.editednotificationid = notificationid;
                LoginService.updatenotification(Login.editednotificationtext, Login.editednotificationid).success(function(response) {
                    cfpLoadingBar.complete();
                    Login.UpdatenotificationCheck = JSON.parse(response);
                    $("#myModal").modal("hide");
                    if (Login.UpdatenotificationCheck == 1) {
                        swal({
                            title: "Success",
                            text: "Notification Updated Successfully"
                        });
                        Login.getallnotifications();
                    } else {
                        swal({
                            title: "Failed!",
                            text: "Notification updation failed"

                        });
                    }
                })
            } else {
                cfpLoadingBar.complete();
                swal({
                    title: "Failed!",
                    text: "Notification Field is empty"

                });
            }
        }

        Login.deletenotification = function(notificationid) {
            cfpLoadingBar.start();
            Login.deletenotificationid = notificationid;
            LoginService.deletenotification(Login.deletenotificationid).success(function(response) {
                cfpLoadingBar.complete();
                    Login.DeletenotificationCheck = JSON.parse(response);
                    $("#myModaldelete").modal("hide");
                    if (Login.DeletenotificationCheck == 1) {
                        swal({
                            title: "Success",
                            text: "Notification Deleted Successfully"
                        });
                        Login.getallnotifications();
                    } else {
                        swal({
                            title: "Failed!",
                            text: "Notification deletion failed"

                        });
                    }
                })
        }
        Login.viewprayer = function(prayerrequestid) {
            cfpLoadingBar.start();
            Login.prayerrequestid = prayerrequestid;
            LoginService.viewuserdetails(Login.prayerrequestid).success(function(response) {
                cfpLoadingBar.complete();
                $("#viewusermodal").modal("show"); 
                $scope.viewusername             = response[0].UserName;
                $scope.viewuseremailaddress      = response[0].UserEmailAddress;
                $scope.viewuserphonenumber      = response[0].UserPhoneNumber;
                $scope.viewuseraddress          = response[0].UserAddress;
                //Login.Getuserdetails = JSON.parse(response);
            })
            
        }

    })
    ($scope.Login);


}]);
